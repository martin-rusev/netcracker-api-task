# NetCracker API Task

Rest API that:
  1) receives Agreement object and stores it into a file with agreement's name
  2) receives file path to agreement saved in previous point and creates Agreement object with all nested products
