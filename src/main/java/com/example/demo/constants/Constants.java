package com.example.demo.constants;

/**
 * This is a class that contains constants that are passed as arguments
 * to different methods that have been called.
 */
public class Constants {
    public static final String AGREEMENT_NAME = "Agreement-";
    public static final String FILE_EXTENSION = ".txt";
    public static final int PRODUCT_NAME_MINIMUM_LENGTH = 3;
    public static final int PRODUCT_SIGN_BY_MINIMUM_LENGTH = 3;
    public static final String PRODUCT_NAME_CHECK_ERROR = String.format("You need to write a name of the product with minimum %d characters!", PRODUCT_NAME_MINIMUM_LENGTH);
    public static final String PRODUCT_PRICE_CHECK_ERROR = "The price must be a positive number!";
    public static final String PRODUCT_SIGN_BY_CHECK_ERROR = String.format("Sign By must be minimum %d characters!", PRODUCT_SIGN_BY_MINIMUM_LENGTH);
    public static final String PARENT_CHECK_ERROR ="It is not allowed to add a child product!";
}
