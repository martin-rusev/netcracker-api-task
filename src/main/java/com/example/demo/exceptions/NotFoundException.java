package com.example.demo.exceptions;

/**
 * This is a custom exception that is thrown when trying to find an entity
 * that does not exist in the records.
 */
public class NotFoundException extends Exception {

    public NotFoundException(String type, String id, String attribute) {
        super(String.format("%s with %s %s was not found in the Repository!",
                type, id, attribute));
    }

}
