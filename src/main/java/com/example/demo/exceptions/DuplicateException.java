package com.example.demo.exceptions;

/**
 * This is a custom exception that is thrown when trying to create
 * an entity that already exists in the records.
 */
public class DuplicateException extends Exception {

    public DuplicateException(String type, String attribute, String name) {
        super(String.format("%s with %s %s already exists!", type, attribute, name));
    }

}
