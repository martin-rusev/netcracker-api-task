package com.example.demo.exceptions;

/**
 * This is a custom exception that is thrown when a given input
 * does not meet the business requirements of this application.
 */
public class InvalidInputException extends Exception {

    public InvalidInputException(String errorMessage) {
        super(errorMessage);
    }
}
