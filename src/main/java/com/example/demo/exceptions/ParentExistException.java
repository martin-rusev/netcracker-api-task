package com.example.demo.exceptions;

import static com.example.demo.constants.Constants.*;

/**
 * This is a custom exception that is thrown when a certain product has a
 * parent product in which it is included.
 */
public class ParentExistException extends Exception {

    public ParentExistException() {
        super(PARENT_CHECK_ERROR);
    }

}
