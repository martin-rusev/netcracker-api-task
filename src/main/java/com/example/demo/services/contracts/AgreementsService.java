package com.example.demo.services.contracts;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.exceptions.ParentExistException;
import com.example.demo.models.Agreement;

import java.io.IOException;

/**
 * This is a class that provides business functionality connected with the
 * Agreement class.
 */
public interface AgreementsService {

    Agreement createAgreement(Agreement agreement) throws IOException, NotFoundException, ParentExistException;

    void saveAgreementFile(Agreement agreement);
}
