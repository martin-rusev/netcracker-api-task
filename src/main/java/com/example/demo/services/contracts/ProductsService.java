package com.example.demo.services.contracts;

import com.example.demo.exceptions.DuplicateException;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.Product;

import java.util.List;

/**
 * This is a class that provides business functionality connected with the
 * Product class.
 */
public interface ProductsService {

    Product getProductByName(String name) throws NotFoundException;

    void createProduct(Product product) throws DuplicateException;

    void updateProduct(Product product);

    void setParentToProduct(Product parent);

}
