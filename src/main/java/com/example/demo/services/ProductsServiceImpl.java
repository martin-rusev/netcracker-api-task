package com.example.demo.services;

import com.example.demo.exceptions.DuplicateException;
import com.example.demo.exceptions.NotFoundException;

import static com.example.demo.helpers.Validator.*;

import com.example.demo.exceptions.ParentExistException;
import com.example.demo.models.Product;
import com.example.demo.repositories.ProductsRepository;
import com.example.demo.services.contracts.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is a class that implements ProductsService interface.
 * Implements a method for getting a product instance by given name of the product,
 * method for creating/saving a product and method for updating a product.
 */
@Service
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public Product getProductByName(String name) throws NotFoundException {
        if (!productsRepository.existsByName(name)) {
            throw new NotFoundException("Product", "name", name);
        }

        return productsRepository.findByName(name);
    }

    @Override
    public void createProduct(Product product) throws DuplicateException {
        if (productsRepository.existsByName(product.getName())) {
            throw new DuplicateException("Product", "name", product.getName());
        }

        productsRepository.saveAndFlush(product);
        setParentToProduct(product);
    }

    @Override
    public void updateProduct(Product product) {
        productsRepository.save(product);
    }

    @Override
    public void setParentToProduct(Product parent) {
        if (parent.getChildren() == null || parent.getChildren().isEmpty()) {
            return;
        }

        for (Product product : parent.getChildren()) {

            List<Product> parents = product.getParents();
            parents.add(parent);

            product.setParents(parents);
            updateProduct(product);
        }

    }
}

