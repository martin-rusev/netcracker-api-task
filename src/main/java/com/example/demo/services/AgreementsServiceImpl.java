package com.example.demo.services;

import static com.example.demo.constants.Constants.FILE_EXTENSION;
import static com.example.demo.helpers.Validator.*;
import static com.example.demo.helpers.Helper.countNumberOfAgreementsLines;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.exceptions.ParentExistException;
import com.example.demo.models.Agreement;
import com.example.demo.models.Product;
import com.example.demo.repositories.AgreementsRepository;
import com.example.demo.services.contracts.AgreementsService;
import com.example.demo.services.contracts.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a class that implements AgreementsService interface.
 * Implements a methods in connection with the creation/saving of an object
 * of the Agreement class.
 */
@Service
public class AgreementsServiceImpl implements AgreementsService {
    private final AgreementsRepository agreementsRepository;
    private final ProductsService productsService;

    @Autowired
    public AgreementsServiceImpl(AgreementsRepository agreementsRepository,
                                 ProductsService productsService) {
        this.agreementsRepository = agreementsRepository;
        this.productsService = productsService;
    }

    @Override
    public Agreement createAgreement(Agreement agreement) throws IOException, NotFoundException, ParentExistException {
        if (isProductHaveParent(agreement.getProducts())) {
            throw new ParentExistException();
        }

        agreementsRepository.saveAndFlush(agreement);
        saveAgreementFile(agreement);

        String[] textData = readAgreementFile(agreement);
        return createAgreementFromReadedFile(textData);

    }

    @Override
    public void saveAgreementFile(Agreement agreement) {
        try {
            FileWriter agreementFile = new FileWriter(agreement.getName() + FILE_EXTENSION);
            BufferedWriter out = new BufferedWriter(agreementFile);

            out.write(agreement.getName() + System.lineSeparator());
            out.write(agreement.getSignedBy() + System.lineSeparator());

            for (Product product : agreement.getProducts()) {
                out.write(product.getName());
                out.newLine();
            }

            out.close();

        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
        }
    }

    public String[] readAgreementFile(Agreement agreement) throws IOException {
        FileReader fileReader = new FileReader(agreement.getName() + FILE_EXTENSION);
        BufferedReader textReader = new BufferedReader(fileReader);

        String[] textData = new String[countNumberOfAgreementsLines(agreement)];

        for (int i = 0; i < textData.length; i++) {
            textData[i] = textReader.readLine();
        }

        textReader.close();
        return textData;
    }

    public Agreement createAgreementFromReadedFile(String[] textData) throws NotFoundException {
        Agreement agreement = new Agreement();
        agreement.setName(textData[0]);
        agreement.setSignedBy(textData[1]);

        List<Product> container = new ArrayList<>();

        for (int i = 2; i < textData.length; i++) {
            container.add(productsService.getProductByName(textData[i]));
        }

        agreement.setProducts(container);

        return agreement;
    }

}
