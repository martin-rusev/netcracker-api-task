package com.example.demo.helpers;

import static com.example.demo.constants.Constants.*;

import com.example.demo.exceptions.InvalidInputException;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.Agreement;
import com.example.demo.models.Product;
import com.example.demo.services.contracts.ProductsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This is a class that expresses the logic of setting a certain data on
 * a particular object's field.
 */
@Component
public class Mapper {
    private final ProductsService productsService;

    @Autowired
    public Mapper(ProductsService productsService) {
        this.productsService = productsService;
    }

    public Agreement toAgreement(String signedBy, List<String> products) throws NotFoundException, InvalidInputException {

        Validator.checkUserAgreementInput(signedBy, products);

        Agreement agreement = new Agreement();
        agreement.setSignedBy(signedBy);

        setAgreementName(agreement);
        setAgreementProducts(agreement, products);

        return agreement;
    }

    public void setAgreementName(Agreement agreement) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String stringDate = formatter.format(date);

        agreement.setName(AGREEMENT_NAME + stringDate);

    }

    public void setAgreementProducts(Agreement agreement, List<String> products) throws NotFoundException {
        List<Product> container = new ArrayList<>();

        for (String product : products) {
            container.add(productsService.getProductByName(product));
        }

        agreement.setProducts(container);
    }

    public Product toProduct(String name, double price, List<String> children) throws NotFoundException, InvalidInputException {

        Validator.checkUserProductInput(name, price);

        Product product = new Product();
        product.setName(name);
        product.setPrice(price);
        setChildrenProducts(product, children);

        return product;
    }

    public void setChildrenProducts(Product product, List<String> children) throws NotFoundException {
        if (children == null || children.isEmpty()) {
            return;
        }

        List<Product> container = new ArrayList<>();

        for (String child : children) {
            container.add(productsService.getProductByName(child));
        }

        product.setChildren(container);
    }

}
