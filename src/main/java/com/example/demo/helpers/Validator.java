package com.example.demo.helpers;

import static com.example.demo.constants.Constants.*;
import com.example.demo.exceptions.InvalidInputException;
import com.example.demo.models.Product;

import java.util.List;

/**
 * This is a class that validates a certain input based on the business requirements
 * of this applications and generally when it is necessary to validate
 * if a certain condition is met.
 */
public class Validator {

    public static boolean isProductHaveParent(List<Product> products) {
        for (Product product:products) {
            if (!product.getParents().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static void checkUserProductInput(String name, double price) throws InvalidInputException {

        if (name == null || name.length() < PRODUCT_NAME_MINIMUM_LENGTH) {
            throw new InvalidInputException(PRODUCT_NAME_CHECK_ERROR);
        }

        if (price <= 0) {
            throw new InvalidInputException(PRODUCT_PRICE_CHECK_ERROR);
        }

    }

    public static void checkUserAgreementInput(String signedBy, List<String>products) throws InvalidInputException {

        if (signedBy == null || signedBy.length() < PRODUCT_SIGN_BY_MINIMUM_LENGTH) {
            throw new InvalidInputException(PRODUCT_SIGN_BY_CHECK_ERROR);
        }

        if (products == null || products.stream().anyMatch(i -> i.length() < PRODUCT_NAME_MINIMUM_LENGTH)) {
            throw new InvalidInputException(PRODUCT_NAME_CHECK_ERROR);
        }

    }

}
