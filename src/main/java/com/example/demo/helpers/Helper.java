package com.example.demo.helpers;

import com.example.demo.models.Agreement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static com.example.demo.constants.Constants.*;

/**
 * This is a class that serves as a helper class when special logic is needed,
 * which should be placed in a separate module.
 */
public class Helper {

    public static int countNumberOfAgreementsLines(Agreement agreement) throws IOException {

        FileReader fileReader = new FileReader(agreement.getName() + FILE_EXTENSION);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        int numberOfLines = 0;

        while ((line = bufferedReader.readLine()) != null) {
            numberOfLines++;
        }
        bufferedReader.close();
        return numberOfLines;

    }

}
