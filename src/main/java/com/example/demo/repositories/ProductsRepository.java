package com.example.demo.repositories;

import com.example.demo.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This is an interface that provides functionality connected with
 * the products in the database.
 */
@Repository
public interface ProductsRepository extends JpaRepository<Product, Integer> {

    Product findByName(String name);

    boolean existsByName(String name);

}
