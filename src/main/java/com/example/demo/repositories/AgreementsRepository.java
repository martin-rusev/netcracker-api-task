package com.example.demo.repositories;

import com.example.demo.models.Agreement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This is an interface that provides functionality connected
 * with the agreements in the database.
 */
@Repository
public interface AgreementsRepository extends JpaRepository<Agreement, Integer> {

}
