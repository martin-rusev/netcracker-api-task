package com.example.demo.controllers;

import com.example.demo.exceptions.InvalidInputException;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.exceptions.ParentExistException;
import com.example.demo.helpers.Mapper;
import com.example.demo.models.Agreement;
import com.example.demo.services.contracts.AgreementsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

/**
 * This is a class that maps HTTP requests relating with the Agreement class
 * to the functions below.
 */
@RestController
@RequestMapping("/api/agreement")
public class AgreementsRestController {
    private final AgreementsService agreementsService;
    private final Mapper mapper;

    @Autowired
    public AgreementsRestController(AgreementsService agreementsService, Mapper mapper) {
        this.agreementsService = agreementsService;
        this.mapper = mapper;
    }

    @PostMapping
    public Agreement createAgreement(@RequestParam("signedBy") String signedBy,
                                     @RequestParam("products") List<String> products) {

        try {
            Agreement agreementToCreate = mapper.toAgreement(signedBy, products);
            return agreementsService.createAgreement(agreementToCreate);

        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (ParentExistException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (InvalidInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}
