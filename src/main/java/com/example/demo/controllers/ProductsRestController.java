package com.example.demo.controllers;

import com.example.demo.exceptions.DuplicateException;
import com.example.demo.exceptions.InvalidInputException;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.helpers.Mapper;
import com.example.demo.models.Agreement;
import com.example.demo.models.Product;
import com.example.demo.services.contracts.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

/**
 * This is a class that maps HTTP requests relating with the Product class
 * to the functions below.
 */
@RestController
@RequestMapping("/api/product")
public class ProductsRestController {
    private final ProductsService productsService;
    private final Mapper mapper;

    @Autowired
    public ProductsRestController(ProductsService productsService, Mapper mapper) {
        this.productsService = productsService;
        this.mapper = mapper;
    }

    @PostMapping
    public Product createProduct(@RequestParam("name") String name,
                                 @RequestParam("price") double price,
                                 @RequestParam(value = "children", required = false) List<String> children) {

        try {
            Product productToCreate = mapper.toProduct(name, price, children);
            productsService.createProduct(productToCreate);
            return productToCreate;

        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

}
